# etc-link
# gnu
http://www.man7.org/linux/man-pages/man2/gettimeofday.2.html  
http://www.man7.org/linux/man-pages/man7/time.7.html  
http://www.man7.org/linux/man-pages/man7/vdso.7.html  
http://man7.org/linux/man-pages/man7/futex.7.html  

# c/cpp/c++
https://en.cppreference.com/w/cpp/container/vector  
https://zh.cppreference.com/w/c/io/fprintf  

# elearning web/lang
https://www.runoob.com/lua/lua-tutorial.html  
https://www.w3cschool.cn/sublimetext/  

# //git
git换行符LF与CRLF转换问题 https://www.cnblogs.com/sdgf/p/6237847.html  
git config --global core.autocrlf false/input/true  
git config --global core.safecrlf true/warn/false  
Sublime Text,View->Line Endings，选Unix，保存;  
https://www.cnblogs.com/fangshenghui/p/5693610.html  

# //gitlab/github--2git-acc
https://blog.csdn.net/px_dn/article/details/89455457 
创建github repository(仓库) https://www.cnblogs.com/siestakc/p/6862446.html  

# git17-hi-memo1
https://github.com/asbcd17z/hi1/blob/master/memo1.txt  
###Google Chrome版本 72.0.3626.119（正式版本）（64 位）, firefox 65.0.1 (64 位)  
https://github.com/TheAlgorithms/Python  
https://github.com/libfuse/  
用Python写一个FUSE（用户态文件系统）文件系统 http://blog.jobbole.com/51268/
https://github.com/qianyubl/WATCH_dojo  
https://github.com/search?l=C&p=5&q=timer+c&type=Repositories  

# markdown/asciidoc/graphviz/plantuml/planttext.com
方便好用的在线UML制作工具：PlantUML https://www.jianshu.com/p/c006dcb1493b  
如何在线编辑显示markdown/plantuml/graphiz? https://segmentfault.com/q/1010000002802979  
http://www.plantuml.com/plantuml/ , https://stackedit.io/ , https://github.com/mikitex70/plantuml-markdown  
https://www.planttext.com/  
https://plantuml.com/zh/  
uml par/UML序列图总结(Loop,Opt,Par和Alt) https://www.cnblogs.com/cy568searchx/p/6227238.html  
eg:程序员绘图工具-Plantuml https://www.jianshu.com/p/30f6a9c06083  https://zhuanlan.zhihu.com/p/76948461  
eg:plantuml使用教程 https://www.cnblogs.com/ningskyer/articles/5397750.html  http://archive.3zso.com/archives/plantuml-quickstart.html  
markdown-stackedit使用 https://www.cnblogs.com/mfmdaoyou/p/6709491.html  
https://www.w3cschool.cn/asciidoc_guide/  
https://github.com/stanzgy/wiki/blob/master/markup/asciidoc-guide.asciidoc  
baidu-asciidoc https://asciidoctor.cn/docs/asciidoc-syntax-quick-reference/  
在线工具 https://tool.lu/asciidoc/  https://tool.lu/markdown/


# end==
